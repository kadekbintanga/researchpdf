from PyPDF2 import PdfWriter, PdfReader, Transformation
import io
from reportlab.pdfgen import canvas


existing_pdf = PdfReader(open("file6.pdf", "rb"))
page1 = existing_pdf.pages[0]

# print(pdf)
output = PdfWriter()

# page 1 image 1
page = existing_pdf.pages[0]
page_width= page.trimbox.right.as_numeric()
page_height = page.trimbox.top.as_numeric()
print(page_width)
print(page_height)
packet = io.BytesIO()
can = canvas.Canvas(packet, pagesize=(page_width, page_height))
can.drawImage("signature_risize2.png", (468.7 /1.2)+18,(page_height - (184.9 / 1.2)) ,mask='auto',anchorAtXY=True, anchor='nw')
can.save()
packet.seek(0)
new_pdf = PdfReader(packet)
page.merge_page(new_pdf.pages[0])


# page 1 image 2
packet = io.BytesIO()
can = canvas.Canvas(packet, pagesize=(page_width, page_height))
can.drawImage("signature_risize1.png",(77.9/1.2), (page_height - ((92.9 / 1.2)+20)),mask='auto',anchorAtXY=True, anchor='nw')
can.save()
packet.seek(0)
new_pdf = PdfReader(packet)
page = existing_pdf.pages[0]
page.merge_page(new_pdf.pages[0])


# page 1 image initial
packet = io.BytesIO()
can = canvas.Canvas(packet, pagesize=(page_width, page_height))
can.drawImage("initial_risize1.png",(170.7), (page_height-(270.8+18)),mask='auto',anchorAtXY=True, anchor='nw')
can.save()
packet.seek(0)
new_pdf = PdfReader(packet)
page = existing_pdf.pages[0]
page.merge_page(new_pdf.pages[0])


# page 1 image qr
packet = io.BytesIO()
can = canvas.Canvas(packet, pagesize=(page_width, page_height))
can.drawImage("qr_risize1.png",147/1.2, (page_height - 656/1.2), mask='auto', anchorAtXY=True, anchor='nw')
can.save()
packet.seek(0)
new_pdf = PdfReader(packet)
page = existing_pdf.pages[0]
page.merge_page(new_pdf.pages[0])
output.add_page(page)



# page 2
# page = existing_pdf.pages[1]
# page_rotation = page.get("/Rotate")
# page_height = page.trimbox.right.as_numeric()
# page_width = page.trimbox.top.as_numeric()
# print(page_height)
# print(page_width)

# x = 120
# y = 400
# x = page_width - x
# y = page_height - y
# if page_rotation == 90:
#     x = x
#     y = -y
#     # swap = page_height
#     # page_height = page_width
#     # page_width = swap
# elif page_rotation == 180:
#     x = -x
#     y = -y
# elif page_rotation == 270:
#     x = -x
#     y = y
#     # swap = page_height
#     # page_height = page_width
#     # page_width = swap

# packet = io.BytesIO()
# can = canvas.Canvas(packet, pagesize=A4)
# can.drawImage("signature2.png", x, y, 70,70, mask='auto')
# can.setPageRotation(0)
# can.save()
# packet.seek(0)
# new_pdf = PdfReader(packet)


# if page_rotation is None:
#     page_rotation = 0
# signature_page = new_pdf.pages[0]
# signature_page.add_transformation(Transformation().rotate(page_rotation))
# #  page.merge_page(signature_page)
# page.merge_page(signature_page)
# output.add_page(page)

# # page 3
# packet = io.BytesIO()
# can = canvas.Canvas(packet, pagesize=A4)
# can.drawImage("signature2.png", 250, 250, 100,100, mask='auto')
# can.setPageRotation(0)
# can.save()
# packet.seek(0)
# new_pdf = PdfReader(packet)
# page = existing_pdf.pages[2]
# page.merge_page(new_pdf.pages[0])
# output.add_page(page)

# finally, write "output" to a real file
output_stream = open("destination.pdf", "wb")
output.write(output_stream)
output_stream.close()