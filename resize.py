from PIL import Image

im = Image.open("initial1.png")
width, height = im.size
print(width)
print(height)

lebar_db = 88
persentase_perubahan = 0
if width > height:
    persentase_perubahan = (lebar_db*100)/width
else:
    persentase_perubahan = (lebar_db*100)/height

new_width = ((width * persentase_perubahan)/100)/1.2
new_height = ((height * persentase_perubahan)/100)/1.2

im1 = im.resize((int(new_width), int(new_height)), Image.Resampling.LANCZOS)
width, height = im1.size
print(width)
print(height)
im1.save("initial_risize1.png","PNG")